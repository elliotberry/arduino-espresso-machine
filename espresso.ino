#include <Wire.h>
#include <Adafruit_SSD1306.h>
#include <Adafruit_GFX.h>

//#include "max6675.h"

//int ktcSO = 5;
//int ktcCS = 6;
////int ktcCLK = 7;

//aMAX6675 ktc(ktcCLK, ktcCS, ktcSO);
// OLED display TWI address
#define OLED_ADDR   0x3C

Adafruit_SSD1306 display(-1);

#if (SSD1306_LCDHEIGHT != 64)
#error("Height incorrect, please fix Adafruit_SSD1306.h!");
#endif
int a = 0;

void setup() {
  // initialize and clear display
  display.begin(SSD1306_SWITCHCAPVCC, OLED_ADDR);
  display.clearDisplay();
  display.display();
}

void loop() {
  display.clearDisplay();
  display.setCursor(1, 1);
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.println("Boiler Temperature?!");
  //display.setCursor(1, 20);
  //display.setTextSize(3);
  //display.print(ktc.readFahrenheit());
  //display.println("F");
  display.display();
  delay(200);

}
